import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "@/styles/common.less"; //引入公共样式
createApp(App).use(router).mount("#app");
